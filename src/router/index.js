import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import AboutView from '@/views/AboutView.vue'
import AvatarView from "@/views/AvatarView.vue";
import ApiView from "@/views/ApiView.vue"
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView,
    },
    {
      path: '/avatar',
      name: 'avatar',
      component: AvatarView
    },

    {
      path: '/api',
      name: 'api',
      component: ApiView
    },
    {
      path: '/People',
      name: 'People',
      component: ApiView
    },
    {
      path: '/Starships',
      name: 'Starships',
      component: ApiView
    },
    {
      path: '/Vehicles',
      name: 'Vehicles',
      component: ApiView
    },
    {
      path: '/Species',
      name: 'Species',
      component: ApiView
    },
    {
      path: '/Planets',
      name: 'planets',
      component: ApiView
    },
  ]
})
export default router
